import time, sys, getopt, os, datetime
import urllib
from tqdm import tqdm
import pandas as pd
import numpy as np
import math
from BaseModule import Timer

def AdditionalData(df):	
	#---------------------------------
	def rsi(price, n=14):
		#---------------------------------
		def rsiCalc(p):
			# subfunction for calculating rsi for one lookback period
			avgGain = p[p>0].sum()/n
			avgLoss = -p[p<0].sum()/n 
			rs = avgGain/avgLoss
			return 100 - 100/(1+rs)
		#---------------------------------
		''' rsi indicator '''
		gain = (price-price.shift(1)).fillna(0) # calculate price gain with previous day, first row nan is filled with 0
		# run for all periods with rolling_apply
		return pd.rolling_apply(gain,n,rsiCalc) 
	#---------------------------------
	def ADX(df, n, n_ADX):  
		i = len(df)-2  
		UpI = []  
		DoI = [] 
		while i >= 0:  
			UpMove = df.get_value(i, 'High') - df.get_value(i+1, 'High')  
			DoMove = df.get_value(i+1, 'Low') - df.get_value(i, 'Low')  
			if 0 < UpMove > DoMove:  
				UpD = UpMove  
			else:
				UpD = 0  
			UpI.append(UpD)  
			if 0 < DoMove > UpMove:  
				DoD = DoMove  
			else:
				DoD = 0  
			DoI.append(DoD)

			i = i - 1   
		i = len(df)-2 
		TR_l = [0]  
		while i >= 0:    
			TR = max((df.get_value(i,'High') - df.get_value(i,'Low')),(abs(df.get_value(i,'High')-df.get_value(i+1,'Close'))),(abs(df.get_value(i,'Low')-df.get_value(i+1,'Close'))))
			TR_l.append(TR)  
			i = i - 1  
		TR_s = pd.Series(TR_l)
		ATR = pd.Series(pd.rolling_mean(TR_s, window = 14))
		UpI = pd.Series(UpI)
		DoI = pd.Series(DoI)
	  
		smoothDXP = 0
		smoothDXN = 0
		i = 0
		while i <= 13:
			smoothDXP += UpI.get_value(i)
			smoothDXN += DoI.get_value(i)
			
			i+=1
			
		SMP=[0,0,0,0,0,0,0,0,0,0,0,0,0,0]
		SMN=[0,0,0,0,0,0,0,0,0,0,0,0,0,0]
		SMP.append(smoothDXP/14)
		SMN.append(smoothDXN/14)
		i = 14
		while i <= len(UpI)-1:
			someP = (SMP[-1]*13 + UpI.get_value(i))/14
			someN = (SMN[-1]*13 + DoI.get_value(i))/14
			
			SMN.append(someN)
			SMP.append(someP)
			
			i+=1
		
		pizda = pd.Series(SMP)
		hyi = pd.Series(SMN)
		pizda = pizda.multiply(100)
		hyi = hyi.multiply(100)
		PosDI = pizda / ATR
		NegDI = hyi / ATR

		DX = (abs(PosDI-NegDI)/(PosDI+NegDI)).multiply(100)
		adx = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
		
		i = 14
		some = 0
		while i <= 27:
			some += DX.get_value(i)
			
			i+=1
		some = some / 14
		i = 28
		while i <= len(DX)-1:
			some = (adx[-1]*13 + DX.get_value(i))/14
			adx.append(some)			
			i+=1
		adx= adx[::-1]
		mypositiv = PosDI.tolist()
		mynegative = NegDI.tolist()
		
		mypositiv = mypositiv[::-1]
		mynegative = mynegative[::-1]
		df['ADX_num'] = pd.Series(adx)
		df['+DI'] = pd.Series(mypositiv)
		df['-DI'] = pd.Series(mynegative)
		df[df == 0] = np.nan
		return df
	#---------------------------------
	#For infographic
	df['RSI_num'] = rsi(df['Close'])
	df = ADX(df,14,14)
	df['RSI'] = 'Neutral'
	df.ix[df.RSI_num >= 70,'RSI'] = 'Overbought'
	df.ix[df.RSI_num <= 30,'RSI'] = 'Oversold'
	
	df['ADX']='N/A'
	df.ix[df.ADX_num < 20,'ADX'] = 'Sideways'
	df.ix[(df.ADX_num > 25) & (df['+DI']>df['-DI']),'ADX'] = 'Bullish'
	df.ix[(df.ADX_num > 25) & (df['+DI']<df['-DI']),'ADX'] = 'Bearish'
	
	df['Scenario']='0'
	df.ix[(df.ADX_num < 20),'Scenario'] = '5'
	df.ix[(df.ADX_num > 25) & (df['+DI']>df['-DI']),'Scenario'] = '4'
	df.ix[(df.ADX_num > 25) & (df['+DI']<df['-DI']),'Scenario'] = '6'
	df.ix[(df.ADX_num < 20) & (df.RSI_num >= 70),'Scenario'] = '2'
	df.ix[(df.ADX_num > 25) & (df['+DI']>df['-DI']) & (df.RSI_num >= 70),'Scenario'] = '1'
	df.ix[(df.ADX_num > 25) & (df['+DI']<df['-DI']) & (df.RSI_num >= 70),'Scenario'] = '3'
	df.ix[(df.ADX_num < 20) & (df.RSI_num <= 30),'Scenario'] = '8'
	df.ix[(df.ADX_num > 25) & (df['+DI']>df['-DI']) & (df.RSI_num <= 30),'Scenario'] = '7'
	df.ix[(df.ADX_num > 25) & (df['+DI']<df['-DI']) & (df.RSI_num <= 30),'Scenario'] = '9'
	
	#GAP
	df['Gain'] = df.Open.shift(1)/df.Close - 1
	df['Trend'] = 'No Trend'
	df['Trend'][(df.Gain >= 0.02)&(df.Gain <= 0.04)] = 'Bullish'
	df['Trend'][(df.Gain <= -0.02)&(df.Gain >= -0.04)] = 'Bearish'
	
	#DAY_X
	df['DayMoveUp'] = (((df['Close']>df['Close'].shift(1)) & True) | ((df['Close']<df['Close'].shift(1)) & False))
	df.ix[df.DayMoveUp == True,'DayMoveUp'] = 'Bullish'
	df.ix[df.DayMoveUp == False,'DayMoveUp'] = 'Bearish'
	
	return df

@Timer
def DownloadData(tickerFile,csvFolderPath):	
	print 'Begin downloading data...'
	#---------------------------
	def make_url(ticker):
		return "http://ichart.finance.yahoo.com/table.csv?s=%s&a=0&b=1&c=2006&d=0&e=1&f=2016&g=d" % (ticker)
	#---------------------------	
	os.system('rm -r '+csvFolderPath)
	os.system('mkdir '+csvFolderPath)
	
	with open(tickerFile) as f:
		for ticker in tqdm(f.readlines()):
			try:
				
				ticker = ticker[:-1]
				csvFile = csvFolderPath + ticker +'.csv'
				if os.path.isfile(csvFile):
					raise LookupError
				urllib.urlretrieve(make_url(ticker), csvFile)
				df = pd.read_csv(csvFile)
				df = df.iloc[::-1]
				df.to_csv(csvFile)
			except KeyError as p:
				print 'Key Error %s'%ticker
				continue
			except pd.parser.CParserError as p:					
				print 'CParserError %s'%ticker
				continue
			except LookupError:
				print '%s already exists!'%ticker
				continue
@Timer
def CalculateData(tickerFile,csvFolderPath):	
	print 'Begin Calculating data...'
	with open(tickerFile) as f:
		for ticker in tqdm(f.readlines()):
			try:
				ticker = ticker[:-1]
				csvFile = csvFolderPath + ticker +'.csv'
				df = pd.read_csv(csvFile,index_col = 0)
				df = AdditionalData(df)
				df.to_csv(csvFile)
			except KeyError as p:
				print 'Key Error %s'%ticker
				continue
			except pd.parser.CParserError as p:					
				print 'CParserError %s'%ticker
				continue

def main():
	tickerFile = 'Tickers/bad.txt'	
	csvFolderPath = 'Tickers/Global/'
	DownloadData(tickerFile,csvFolderPath)
	#CalculateData(tickerFile,csvFolderPath)

main()
