import dayX
import pandas as pd
from tqdm import tqdm
import time
from BaseModule import *
from math import fabs

@Timer
def TesterNew(checker):
	#tickers = pd.read_csv('fast_DayX_Forecasts_2010-2014.csv')
	tickers = pd.read_csv('DayX_Forecasts_2010-2014.csv')
	#tickers = tickers.ix[(tickers.Signal >= 2) & (tickers['d1 Sharpe'] >= 1) & (tickers['d1 AVG'] >= 0.005),'Ticker'].unique()
	tickers = tickers['Ticker'].unique()
	print len(tickers)
	final = pd.DataFrame(columns = ['Date','Ticker','AVG','Max','Close', 'Sharp','Signal', 'Days'])
	
	def Counter(f, df, today, ticker, stop_loss):
		final = pd.Series(index = ['Date','Ticker','AVG','Max','Close', 'Sharp','Signal', 'Days'])
		final['Date'] = today['Date']
		final['Ticker'] = ticker		
		final['AVG'] = float(df['d1 AVG'])		
		final['Close'] = final['AVG']
		final['Sharp'] = df['d1 Sharpe']
		final['Signal'] = df['Signal']
		final['Days'] = df['Days']
		action = str(df['Action'])
		if action == 'Short':
			final['Max'] = 1 - float(today['Low'])/float(today['Open'])
			if 1 - float(today['High'])/float(today['Open']) < stop_loss:
				final['Close'] = stop_loss
			else:
				if float(final['AVG']) > float(final['Max']):
					final['Close'] = 1 - float(today['Close'])/float(today['Open'])
					
		elif action == 'Long':
			final['Max'] = float(today['High'])/float(today['Open']) - 1
			if float(today['Low'])/float(today['Open']) - 1 < stop_loss:
				final['Close'] = stop_loss
			else:
				if float(final['AVG']) > float(final['Max']):
					final['Close'] = float(today['Close'])/float(today['Open']) - 1	
								
		f = f.append(final, ignore_index = True)		
		return f		
		
	def Analizator(df):
		if len(df)==0:
			return None	
		df = df[df['d1 Sharpe'] >= 1]		
		if len(df)==0:	
			return None
		df = df[df['Signal'] >= 2]		
		if len(df)==0:	
			return None
		df = df[df['d1 AVG'] >= 0.005]
		if len(df)==0:	
			return None
		#df = df.sort('Signal', ascending = False)
		#df = df.loc[df['Days'].idxmax()]
		df = df.loc[df['Signal'].idxmax()]	
		return df
		
	for i in tqdm(range(1,252)):
		table = pd.DataFrame()
		for ticker in tickers:
			df = pd.read_csv('Tickers/Global/'+ticker+'.csv')
			index = len(df)-252+i
			ans = checker.checker(ticker,df[:index])
			if ans is not None:
				table = table.append(ans, ignore_index = True)

		if table is not None: 
			table = Analizator(table)
			if table is not None:
				ticker = str(table['Ticker'])		
				df = pd.read_csv('Tickers/Global/'+ticker+'.csv')
				today = df.loc[len(df)-252+i]
				final = Counter(final, table, today,ticker,-0.02)
				print final
				final.to_csv('final.csv')
		else:			
			print 'table is none!'
	lt = []
	lt.append((final['Close'].values))
	print sharpCount(lt)	
		
TesterNew(dayX)



#final = pd.read_csv('final.csv')
#lt = []
#lt.append((final['Close'].values))
#print sharpCount(lt)
