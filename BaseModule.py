import pandas as pd
import numpy as np
from math import *
import time

def Timer(f):
	def tmp(*args, **kwargs):		
		t = time.time()
		ans = f(*args, **kwargs)
		print '\tComplite in: %f min.'%((time.time()-t)/60)
		return ans		
	return tmp
	
def sharpCount(s):
	res =pd.DataFrame(s)
	ans = pd.DataFrame()	
	ans['STD']=np.std(res,axis=1,ddof=0)
	ans['AVG']=res.mean(axis=1)
	ans['Sharp']=1
	ans['Sharp']=ans['AVG']/ans['STD']*sqrt(250)	
	return ans

def sharp(x,my_scenario = -1):
	df=pd.DataFrame(x,columns=['d1','d2','d3','scen'])
	final_table = pd.DataFrame()
	little_table1=[]
	little_table2=[]
	little_table3=[]
	
	if my_scenario >= 0:
		df = df[df.scen==my_scenario]
		
	if len(df) != 0:
		
		little_table1.append((df['d1'].values))
		little_table2.append((df['d2'].values))
		little_table3.append((df['d3'].values))
	
		little_table1 = pd.DataFrame(sharpCount(little_table1))
		little_table2 = pd.DataFrame(sharpCount(little_table2))
		little_table3 = pd.DataFrame(sharpCount(little_table3))

		final_table['d1 AVG']= little_table1['AVG']
		final_table['d1 Sharpe']= little_table1['Sharp']
		final_table['d2 AVG']= little_table2['AVG']
		final_table['d2 Sharpe']= little_table2['Sharp']
		final_table['d3 AVG']= little_table3['AVG']
		final_table['d3 Sharpe'] = little_table3['Sharp']
		final_table['Signal'] = len(df)
	return final_table

def gain(df,i,flag, stop_loss):
	#stop_loss = -0.02
	days = []
	i+=1
	if flag == True: #Long
		#Day 1
		if ((df.Low.ix[i]/df.Open.ix[i]-1) < stop_loss): 
			days.append(stop_loss)
		else: days.append(df.Close.ix[i]/df.Open.ix[i]-1)
		#Day 2	
		if ((df.Low.ix[i+1]/df.Open.ix[i+1]-1) < stop_loss): 
			days.append(stop_loss)
		else: days.append(df.Close.ix[i+1]/df.Open.ix[i+1]-1)
		#Day 3	
		if ((df.Low.ix[i+2]/df.Open.ix[i+2]-1) < stop_loss): 
			days.append(stop_loss)	
		else: days.append(df.Close.ix[i+2]/df.Open.ix[i+2]-1)			
		days.append(df.Scenario.ix[i-1])
	elif flag == False: #Short
		#Day 1
		if ((1-df.High.ix[i]/df.Open.ix[i]) < stop_loss):
			days.append(stop_loss)
		else: days.append(1-df.Close.ix[i]/df.Open.ix[i])
		#Day 2	
		if ((1-df.High.ix[i+1]/df.Open.ix[i+1]) < stop_loss):
			days.append(stop_loss)
		else: days.append(1-df.Close.ix[i+1]/df.Open.ix[i+1])
		#Day 3	
		if ((1-df.High.ix[i+2]/df.Open.ix[i+2]) < stop_loss):
			days.append(stop_loss)
		else: days.append(1-df.Close.ix[i+2]/df.Open.ix[i+2])			
		days.append(df.Scenario.ix[i-1])
	return days
