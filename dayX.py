import pandas as pd
import numpy as np
from math import *
from BaseModule import *

def ScanHistory(table, today_trend, d):
	if today_trend == 'Bullish':
		trend = True
	elif today_trend == 'Bearish':
		trend = False
	total_gain = []	
	table = table.reset_index(drop = True)
	for i,row in table.iterrows():
		if i == len(table)-4:
			break
		if GetSignal(table, today_trend, i, d):
			my_gain = []
			my_gain = gain(table.ix[i:i+3],i,trend,-0.02)
			total_gain.append(my_gain)
	return total_gain	

def GetSignal(table, trend, i, days = -1):
	if trend == 'Bullish':
		if table.RSI.ix[i] != 'Oversold':
			return False
	elif trend == 'Bearish':
		if table.RSI.ix[i] != 'Overbought':
			return False
	d = 0		
	while i > 0:
		if (table.DayMoveUp.ix[i] == trend):
			d += 1
		else:break
		i -= 1
		
	if days < 0:
		return d
		
	if d == days:
		return True	
		
	return False

def OutputAnswer(ans, today_trend, d):
	if today_trend == 'Bullish':
		ans['Go up?'] = 'Up'
		ans['Action'] = 'Long'
	elif today_trend == 'Bearish':
		ans['Go up?'] = 'Down'
		ans['Action'] = 'Short'
	ans['Days'] = d
	return ans
	
def GetForecast	(ticker,today_trend,days, today_scen = -1):
	table = pd.read_csv('DayX_Forecasts_2010-2014.csv')
	return table[(table.Ticker == ticker) & (table.Scenario == today_scen) & (table.Trend == today_trend) & (table.Days == days)]
	
def checker(ticker, table):
	i = len(table)-1
	today_trend = table.DayMoveUp.ix[i]
	today_rsi = table.RSI.ix[i]
	d = GetSignal(table,today_trend,i,-1)
	if d > 5:
		ans = GetForecast(ticker, today_trend, d)
	else:
		return None
	if ans.empty or int(ans['Signal']) == 0:
		return None
	return OutputAnswer(ans,today_trend, d)
	

def tester(ticker,today_trend, days):
	table = pd.read_csv('Tickers/Global/'+ticker+'.csv')	
	table = table[1007:len(table)-252]
	final = pd.DataFrame()
	total_gain = []
	total_gain = ScanHistory(table, today_trend, days)
	ans=sharp(total_gain)
	ans['Ticker'] = ticker
	ans['Trend'] = today_trend
	ans['Days'] = days
	ans['Scenario'] = -1
	return ans

#print tester('FCX','Bullish',5)
