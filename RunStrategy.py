import dayX
from tqdm import tqdm
import pandas as pd

def main(strategy):
	Path_tickersFile = 'Tickers/tickers.txt'
	Path_csvFile = 'Tickers/Global'
	tickers = []
	test = pd.DataFrame()
	final = pd.DataFrame()
	
	with open(Path_tickersFile) as f:
		for ticker in f.readlines():
			ticker = ticker[:-1]
			tickers.append(ticker)	
			
	for ticker in tqdm(tickers):
		for days in range(6,9):
			test = strategy.tester(ticker,'Bullish',days)
			final = final.append(test,ignore_index = True)
			test = strategy.tester(ticker,'Bearish',days)
			final = final.append(test,ignore_index = True)
			#print final
	#final.to_csv('fast_DayX_Forecasts_2010-2014.csv')	
			final.to_csv('DayX_Forecasts_2010-2014.csv')	
	print final
	
main(dayX)
